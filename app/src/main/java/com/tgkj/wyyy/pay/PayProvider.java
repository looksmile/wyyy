package com.tgkj.wyyy.pay;

import android.content.Context;

abstract public class PayProvider {
    //用于平台统计付费来自哪个运营商
    public static final int FROM_Alipay = 1; //支付宝支付
    public static final int FROM_WechatPay = 2;//微信支付
    public static final int FROM_PPPay = 3;//PP钱包支付

    protected int smsType;

    abstract public void pay(Context context, PayItem item);

    abstract public int getProviderID();

    abstract public void setProviderID(int val);

    abstract public String getSDKProvider();

    public String getOutTradeNo() {
        return null;
    }
}
