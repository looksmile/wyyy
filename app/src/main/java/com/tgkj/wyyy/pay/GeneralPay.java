package com.tgkj.wyyy.pay;

import android.content.Context;
import android.util.Log;

import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.dialog.PayDialog;
import com.tgkj.wyyy.events.PlaceOrderEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.Vip;
import com.tgkj.wyyy.jobs.PlaceOrderJob;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/3/1.
 */
public class GeneralPay {

    private static volatile GeneralPay instance;

    private String orderId;
    private Context context;
    private Vip vip;

    private GeneralPay() {
        EventBus.getDefault().register(this);
    }


    public static GeneralPay getInstance() {
        if (instance == null) {
            synchronized (GeneralPay.class) {
                if (instance == null) {
                    instance = new GeneralPay();
                }
            }
        }
        return instance;
    }

    public void showPayDialog(Context context, int payType){
        this.orderId = generateOrderID();
        this.context = context;
        if(Constant.QUA_YEAR == payType){
            vip = DataCenter.getInstance().vips.get(1);
        }else if(Constant.HALF_YEAR == payType){
            vip = DataCenter.getInstance().vips.get(0);
        }
        PayDialog.Builder builder = new PayDialog.Builder(context);
        builder.setAlipayListener(new PayDialog.OnAlipayListener() {
            @Override
            public void onAlipayClick() {
                MyApplication.getJobManager().addJobInBackground(new PlaceOrderJob(vip, orderId, Constant.PAY_TYPE_ALIPAY));
            }
        }).setWXpayListener(new PayDialog.OnWXpayListener() {
            @Override
            public void onWXpayClick() {
                MyApplication.getJobManager().addJobInBackground(new PlaceOrderJob(vip, orderId, Constant.PAY_TYPE_WXPAY));
            }
        }).setPPPayListener(new PayDialog.OnPPPayListener() {
            @Override
            public void onPPPayClick() {
                MyApplication.getJobManager().addJobInBackground(new PlaceOrderJob(vip, orderId, Constant.PAY_TYPE_PPPAY));
            }
        });
        builder.build().show();
    }

    private String generateOrderID() {
        Locale loc = Locale.getDefault();
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss", loc);
        Date date = new Date();
        String key = format.format(date);

        java.util.Random r = new java.util.Random();
        key += r.nextInt();
        key = key.substring(0, 15);
        key += "_wyyy";
        return key.replace("-", "_");
    }

    private void doPay(PayProvider payProvider, int callBackType) {

        payProvider.pay(context, new PayItem((int) (vip.getPrice() * 100), orderId, vip.getVipName(), null, callBackType));

    }

    public void onEventMainThread(PlaceOrderEvent event){
        if(event.isSuccess){
            switch (event.payType) {
                case Constant.PAY_TYPE_ALIPAY:
                    doPay(AlipayProvider.getInstance(), PayItem.alipay_callback);
                    break;
                case Constant.PAY_TYPE_PPPAY:
                    doPay(PPPayProvider.getInstance(), PayItem.pppay_callback);
                    break;
                case Constant.PAY_TYPE_WXPAY:
                    doPay(WXPayProvider.getInstance(), PayItem.wxpay_callback);
                    break;
            }
        }
    }
}
