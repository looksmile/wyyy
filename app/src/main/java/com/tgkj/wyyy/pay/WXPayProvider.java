package com.tgkj.wyyy.pay;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.util.Xml;

import com.tgkj.wyyy.events.EmptyEvent;
import com.tgkj.wyyy.events.PayEvent;
import com.tgkj.wyyy.events.WXPayEvent;
import com.tgkj.wyyy.pay.wxpay.WXStringUtil;
import com.tgkj.wyyy.pay.wxpay.WXhttpUtil;
import com.tgkj.wyyy.pay.wxpay.WXinMD5;
import com.tgkj.wyyy.pay.wxpay.WXpayKeys;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.greenrobot.event.EventBus;

/**
 * Created by lenovo on 2015/9/21.
 */
public class WXPayProvider extends PayProvider {
    private static WXPayProvider instance;
    private IWXAPI api;
    private PayReq req;
    private PayItem item;

    public static WXPayProvider getInstance() {
        if (instance == null) {
            synchronized (WXPayProvider.class) {
                if (instance == null) {
                    instance = new WXPayProvider();
                    EventBus.getDefault().register(instance);
                }
            }
        }
        return instance;
    }

    public void onEventMainThread(WXPayEvent event) {
        if (event.isSuccess()) {
            EventBus.getDefault().post(new PayEvent(true, null, item.getBuyObj()));
        } else {
            EventBus.getDefault().post(new PayEvent(false, event.getErrMsg(), item.getBuyObj()));
        }
    }

    @Override
    public void pay(final Context context, final PayItem item) {
        this.item = item;
        boolean isWeiXinInstalled = false;
        boolean isWeiXinSupport = false;
        boolean isWeiXinSupportFriends = false;

        //检测微信是否安装

        api = WXAPIFactory.createWXAPI(context, null);
        if (api.isWXAppInstalled()) {
            isWeiXinInstalled = true;
        }
        if (api.isWXAppSupportAPI()) {
            isWeiXinSupport = true;
        }
        if (api.getWXAppSupportAPI() >= 0x21020001) {
            isWeiXinSupportFriends = true;
        }
        if (!isWeiXinInstalled || !isWeiXinSupportFriends || !isWeiXinSupport) {
            showInstalledWXDialog(!isWeiXinInstalled, context);
            EventBus.getDefault().post(new PayEvent(false, "微信未安装", item.getBuyObj()));
            return;
        }
        req = new PayReq();
        api.registerApp(WXpayKeys.WXIN_APP_ID);

        new Thread(new Runnable() {
            @Override
            public void run() {
                //1.预下单
                String url = String.format("https://api.mch.weixin.qq.com/pay/unifiedorder");
                String entity = genPlaceOrderArgs();
                byte[] buf = WXhttpUtil.httpPost(url, entity);
                String content = new String(buf);
                Map<String, String> xml = decodeXml(content);
                String strReturn = xml.get("return_code");
                if (!WXStringUtil.equals(strReturn, "SUCCESS")) {
                    //失败
                    String strMsg = xml.get("return_msg");
                    EventBus.getDefault().post(new PayEvent(false, strMsg, item.getBuyObj()));
                    return;
                }
                //2.发起支付
                genPayReqAndPay(xml);

            }
        }).start();
    }

    @Override
    public int getProviderID() {
        return 0;
    }

    @Override
    public void setProviderID(int val) {

    }

    @Override
    public String getSDKProvider() {
        return "微信支付";
    }

    private void showInstalledWXDialog(boolean isInstall, final Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("没有安装");
        String msg, select;
        if (isInstall) {
            msg = "您的手机上还没有安装微信，还无法使用此功能，使用微信可以方便的把您喜欢的作品分享给好友";
            select = "下载微信";
        } else {
            msg = "您当前的微信版本过低，无法支持此功能，请更新微信至最新版本。";
            select = "更新微信";
        }

        dialog.setMessage(msg);
        dialog.setPositiveButton(select, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse("http://weixin.qq.com ");
                intent.setData(content_url);
                context.startActivity(intent);
            }
        });
        dialog.setNegativeButton("取消",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.show();

    }

    private Handler handler = new Handler();

    private void genPayReqAndPay(Map<String, String> result) {
        req.appId = WXpayKeys.WXIN_APP_ID;
        req.partnerId = WXpayKeys.WXIN_MCH_ID;
        req.prepayId = result.get("prepay_id");
        req.packageValue = "prepay_id=" + result.get("prepay_id");
        req.nonceStr = genNonceStr();
        req.timeStamp = String.valueOf(System.currentTimeMillis() / 1000);

        List<NameValuePair> signParams = new LinkedList<NameValuePair>();
        signParams.add(new BasicNameValuePair("appid", req.appId));
        signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
        signParams.add(new BasicNameValuePair("package", req.packageValue));
        signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
        signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
        signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));

        req.sign = genAppSign(signParams);


        boolean bSucc = api.registerApp(WXpayKeys.WXIN_APP_ID);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new EmptyEvent());
            }
        }, 2000);
        if (bSucc) {
            boolean res = api.sendReq(req);
        } else {
            EventBus.getDefault().post(new PayEvent(false, "registerApp failed", item.getBuyObj()));
        }
    }


    private String genAppSign(List<NameValuePair> params) {
        StringBuilder strTemp = new StringBuilder();
        for (int i = 0; i < params.size(); i++) {
            strTemp.append(params.get(i).getName());
            strTemp.append('=');
            strTemp.append(params.get(i).getValue());
            strTemp.append('&');
        }
        strTemp.append("key=");
        strTemp.append(WXpayKeys.WXIN_KEY);
        String appSign = WXinMD5.getMessageDigest(strTemp.toString().getBytes());
        return appSign;
    }

    private String genPackageSign(List<NameValuePair> params) {
        StringBuilder strTemp = new StringBuilder();
        for (int i = 0; i < params.size(); i++) {
            strTemp.append(params.get(i).getName());
            strTemp.append('=');
            strTemp.append(params.get(i).getValue());
            strTemp.append('&');
        }
        strTemp.append("key=");
        strTemp.append(WXpayKeys.WXIN_KEY);
        String packageSign = WXinMD5.getMessageDigest(strTemp.toString().getBytes()).toUpperCase();
        return packageSign;
    }

    private String genNonceStr() {
        Random random = new Random();
        return WXinMD5.getMessageDigest(String.valueOf(random.nextInt(10000)).getBytes());
    }

    private String genPlaceOrderArgs() {
        try {
            String nonceStr = genNonceStr();
            String StrBody = item.getPayDescription();
            StrBody = StrBody.replace('\r', ' ');
            StrBody = StrBody.replace('\n', ' ');
            List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
            packageParams.add(new BasicNameValuePair("appid", WXpayKeys.WXIN_APP_ID));//微信分配的应用APPID
            packageParams.add(new BasicNameValuePair("body", StrBody));//商品描述 填写中文有误，先写死
            packageParams.add(new BasicNameValuePair("mch_id", WXpayKeys.WXIN_MCH_ID));//微信分配的微信支付商户号
            packageParams.add(new BasicNameValuePair("nonce_str", nonceStr));//随机字符串
            packageParams.add(new BasicNameValuePair("notify_url", item.getCallbackUrl()));//通知地址
            packageParams.add(new BasicNameValuePair("out_trade_no", item.getOrderID())); //商户订单号
            packageParams.add(new BasicNameValuePair("spbill_create_ip", "8.8.8.8"));//终端IP
//            packageParams.add(new BasicNameValuePair("total_fee", String.valueOf(item.getPrice())));//总金额,单位：分
            packageParams.add(new BasicNameValuePair("total_fee", String.valueOf(1)));//test时用 1分钱
            packageParams.add(new BasicNameValuePair("trade_type", "APP"));//交易类型
            String sign = genPackageSign(packageParams);
            packageParams.add(new BasicNameValuePair("sign", sign));
            return toXml(packageParams);
        } catch (Exception e) {
            return null;
        }
    }

    private String toXml(List<NameValuePair> params) {
        StringBuilder strTemp = new StringBuilder();
        strTemp.append("<xml>");
        for (int i = 0; i < params.size(); i++) {
            strTemp.append("<" + params.get(i).getName() + ">");
            strTemp.append(params.get(i).getValue());
            strTemp.append("</" + params.get(i).getName() + ">");
        }
        strTemp.append("</xml>");
        return strTemp.toString();
    }


    private Map<String, String> decodeXml(String content) {

        try {
            Map<String, String> xml = new HashMap<String, String>();
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(content));
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {

                String nodeName = parser.getName();
                switch (event) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        if ("xml".equals(nodeName) == false) {
                            //实例化student对象
                            xml.put(nodeName, parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                }
                event = parser.next();
            }

            return xml;
        } catch (Exception e) {
            Log.e("orion", e.toString());
        }
        return null;

    }

}
