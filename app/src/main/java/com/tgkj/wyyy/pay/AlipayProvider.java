package com.tgkj.wyyy.pay;

import android.app.Activity;
import android.content.Context;

import com.alipay.sdk.app.PayTask;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.events.PayEvent;
import com.tgkj.wyyy.pay.alipay.AliPayKeys;
import com.tgkj.wyyy.pay.alipay.Result;
import com.tgkj.wyyy.pay.alipay.Rsa;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import de.greenrobot.event.EventBus;

public class AlipayProvider extends PayProvider {
    private static final String TAG = "AlipayProvider";
    private volatile static AlipayProvider sInstance;
    private Context ctx;

    private AlipayProvider() {
        smsType = 0;
    }

    public static AlipayProvider getInstance() {
        if (sInstance == null) {
            synchronized (AlipayProvider.class) {
                if (sInstance == null) {
                    sInstance = new AlipayProvider();
                }
            }
        }
        return sInstance;
    }


    public void pay(final Context context, final PayItem item) {
        ctx = context;
        String info = getNewOrderInfo(item);
        String sign = Rsa.sign(info, AliPayKeys.PRIVATE);
        try {
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String payInfo = info + "&sign=\"" + sign + "\"&" + getSignType();
        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                //先向袁泉服务器下单

                // 构造PayTask 对象
                PayTask alipay = new PayTask((Activity) context);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo);
                Result resultObj = new Result(result);
                resultObj.parseResult();
                if (resultObj.resultStatus != null
                        && resultObj.resultStatus.contains("9000")
                        && resultObj.isSignOk == true) {
                    EventBus.getDefault().post(new PayEvent(true, null, item.getBuyObj()));
                } else {
                    EventBus.getDefault().post(new PayEvent(false, resultObj.resultStatus, item.getBuyObj()));
                }

            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @Override
    public int getProviderID() {
        return smsType;
    }

    @Override
    public void setProviderID(int val) {
        smsType = val;
    }

    @Override
    public String getSDKProvider() {
        return "支付宝";
    }


    private String getNewOrderInfo(PayItem payItem) {
        String price = String.valueOf(((float) payItem.getPrice()) / 100);
        StringBuilder sb = new StringBuilder();
        sb.append("partner=\"");
        sb.append(AliPayKeys.DEFAULT_PARTNER);
        sb.append("\"&out_trade_no=\"");
        sb.append(payItem.getOrderID());
        sb.append("\"&subject=\"");
        sb.append(ctx.getResources().getString(R.string.app_name));
        sb.append("\"&body=\"");
        sb.append(payItem.getPayDescription());
        sb.append("\"&total_fee=\"");
//        sb.append("0.01");//测试时用一分钱
        sb.append(price);
        sb.append("\"&notify_url=\"");

        // 网址需要做URL编码
        sb.append(URLEncoder.encode(payItem.getCallbackUrl()));
        sb.append("\"&service=\"mobile.securitypay.pay");
        sb.append("\"&_input_charset=\"UTF-8");
        sb.append("\"&return_url=\"");
        sb.append(URLEncoder.encode("http://m.alipay.com"));
        sb.append("\"&payment_type=\"1");
        sb.append("\"&seller_id=\"");
        sb.append(AliPayKeys.DEFAULT_SELLER);

        // 如果show_url值为空，可不传
        // sb.append("\"&show_url=\"");
        sb.append("\"&it_b_pay=\"1m");
        sb.append("\"");

        return new String(sb);
    }


    private String getSignType() {
        return "sign_type=\"RSA\"";
    }

}
