package com.tgkj.wyyy.pay.wxpay;

import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * @auther: wxn
 * @since: 13-4-8 下午3:20
 */
public class WXStringUtil {
    //字符串是否为空
    public final static boolean isBlank(String str) {
        if (str == null || str.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public final static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public final static boolean equals(String left, String right) {
        try {
            return left.equals(right);
        } catch (Exception e) {
            return false;
        }
    }

    public final static boolean equalsIgnoreCase(String left, String right) {
        try {
            return left.equalsIgnoreCase(right);
        } catch (Exception e) {
            return false;
        }
    }

    public final static String sub(String str, int start, int end) {
        try {
            return str.substring(start, end);
        } catch (Exception e) {
            return "";
        }
    }

    public final static boolean contains(String str, String sub) {
        try {
            return str.contains(sub);
        } catch (Exception e) {
            return false;
        }
    }

    public final static boolean endWith(String str, String sub) {
        try {
            return str.endsWith(sub);
        } catch (Exception e) {
            return false;
        }
    }

    //解析ip地址返回字符串
    public static String intToIp(int i) {
        return (i & 0xFF) + "." +
                ((i >> 8) & 0xFF) + "." +
                ((i >> 16) & 0xFF) + "." +
                (i >> 24 & 0xFF);
    }

    //使用GPRS获取地址
    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            //Log.e("WifiPreference IpAddress", ex.toString());
        }
        return null;
    }

    public static int getMixLen(String s) {
        if (null == s) {
            return 0;
        }
        return s.getBytes().length;
    }
}
