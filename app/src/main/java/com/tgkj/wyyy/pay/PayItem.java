package com.tgkj.wyyy.pay;

import com.tgkj.wyyy.BuildConfig;
import com.tgkj.wyyy.Constant;

/**
 * Created by cary on 8/13/14.
 */
public class PayItem {
    public static final int alipay_callback = 1; //支付宝回调
    public static final int wxpay_callback = 2;//微信回调
    public static final int pppay_callback = 3;//PP钱包回调

    private int price;//单位：分
    private String orderID;
    private String payDescription;
    private String callbackUrl;//回调地址
    private Object buyObj;

    public PayItem(int price, String orderID, String goodsName, Object buyObj, int callbackType) {
        if (BuildConfig.DEBUG || Constant.ForTest) {
            this.price = 1;
        } else {
            this.price = price;
        }
        this.orderID = orderID;
        this.payDescription = goodsName;
        this.buyObj = buyObj;
        switch (callbackType) {
            case alipay_callback:
                setCallbackUrl(String.format("%s%s", Constant.ServerUrl, "V2-3-1/AlipayCallBack.aspx"));
                break;
            case wxpay_callback:
                setCallbackUrl(String.format("%s%s", Constant.ServerUrl, "V2-3-1/WechatPayCallBack.aspx"));
                break;
            case pppay_callback://pp钱包###
                setCallbackUrl(String.format("%s%s", Constant.ServerUrl, "V2-3-1/PPPayCallBack.aspx"));
                break;
            default:

        }
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPayDescription() {
        return payDescription;
    }

    public void setPayDescription(String payDescription) {
        this.payDescription = this.payDescription;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public Object getBuyObj() {
        return buyObj;
    }
}
