package com.tgkj.wyyy.pay;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.tgkj.wyyy.events.PayEvent;

import java.util.HashMap;

import cn.paypalm.pppayment.PPInterface;
import cn.paypalm.pppayment.global.ResponseDataToMerchant;
import de.greenrobot.event.EventBus;

public class PPPayProvider extends PayProvider implements ResponseDataToMerchant {

    private volatile static PPPayProvider sInstance;

    private String strMerId = "2015082515";// 商户ID
    private String strReserve = "sdk2.2"; // 保留字段
    private PayItem mPayItem;

    private PPPayProvider() {
        smsType = 0;
    }

    public static PPPayProvider getInstance() {
        if (sInstance == null) {
            synchronized (AlipayProvider.class) {
                if (sInstance == null) {
                    sInstance = new PPPayProvider();
                }
            }
        }
        return sInstance;
    }

    @Override
    public void pay(final Context context, final PayItem item) {
        mPayItem = item;
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String deviceId = telephonyManager.getDeviceId();
        String price = String.valueOf(item.getPrice());
        startPP(context,
                item.getOrderID(),
                "",
                strMerId,
                deviceId,
                price,
//                "1",//测试时1分钱
                "100001",
                item.getCallbackUrl(),
                item.getPayDescription(),
                null,
                strReserve,
                PPPayProvider.this);
    }

    @Override
    public int getProviderID() {
        return smsType;
    }

    @Override
    public void setProviderID(int val) {
        smsType = val;
    }

    @Override
    public String getSDKProvider() {
        return "PP钱包";
    }

    /**
     * @param order                  订单号 注:非空
     * @param phone                  手机号 注:非空
     * @param merchantId             商户编号 注:非空
     * @param merchantUserId         商户用户ID 注:非空
     * @param payAmt                 支付金额 注:单位是 分 , 非空
     * @param productId              产品ID 注: 固定传 100001
     * @param notifyUrl              回调接口 注:非空 特别注意!这个必须填写且正确，这个是用于异步通知回调你们商户的，如果填写错误没有回调成功就是你们的问题!
     * @param orderDesc              订单描述(传入商品名称) 注:非空
     * @param reserve                保留字段
     * @param responseDataToMerchant 回调接口(用于向商户返回支付结果) 注:非空
     */

    private void startPP(Context context, String order, String phone,
                         String merchantId, String merchantUserId, String payAmt,
                         String productId, String notifyUrl, String orderDesc,
                         HashMap<String, String> userInfo, String reserve,
                         ResponseDataToMerchant responseDataToMerchant) {

        // 调用PP支付
        PPInterface.startPPPayment(context, order, phone, merchantId,
                merchantUserId, payAmt, productId,
                "BANKCARD_PAY", notifyUrl, orderDesc, userInfo,
                reserve, responseDataToMerchant);
    }


    public void responseData(int arg0, String arg1) {
        switch (arg0) {
            case 0:
                break;
            case ResponseDataToMerchant.RESULT_PAYCODE_OK:
                EventBus.getDefault().post(new PayEvent(true, null, mPayItem.getBuyObj()));
                break;
            case ResponseDataToMerchant.RESULT_PAYCODE_ERROR:
                EventBus.getDefault().post(new PayEvent(false, arg1, mPayItem.getBuyObj()));
                break;
        }
    }
}
