package com.tgkj.wyyy.events;

/**
 * Created by Administrator on 2015/12/23.
 */
public class GetVipItemEvent {
    public boolean isSuccess;
    public String errMsg;

    public GetVipItemEvent(boolean isSuccess, String errMsg) {
        this.isSuccess = isSuccess;
        this.errMsg = errMsg;
    }

}
