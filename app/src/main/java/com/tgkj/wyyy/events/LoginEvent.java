package com.tgkj.wyyy.events;

/**
 * Created by apple on 16/3/8.
 */
public class LoginEvent {
    public boolean isSuccess = false;
    public String errMsg;
    public LoginEvent(boolean isSuccess, String errMsg){
        this.isSuccess = isSuccess;
        this.errMsg = errMsg;
    }
}
