package com.tgkj.wyyy.events;

/**
 * Created by Administrator on 2016/3/3.
 */
public class HomeboyPageEvent {
    public boolean isSuccess = false;
    public String errMsg;
    public HomeboyPageEvent(boolean isSuccess, String errMsg){
        this.isSuccess = isSuccess;
        this.errMsg = errMsg;
    }
}
