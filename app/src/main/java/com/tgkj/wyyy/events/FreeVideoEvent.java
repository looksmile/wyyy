package com.tgkj.wyyy.events;

/**
 * Created by apple on 16/3/9.
 */
public class FreeVideoEvent {
    public boolean isSuccess = false;
    public String errMsg;
    public FreeVideoEvent(boolean isSuccess, String errMsg){
        this.isSuccess = isSuccess;
        this.errMsg = errMsg;
    }
}
