package com.tgkj.wyyy.events;

public class PayEvent {

    private boolean isPaySuccess = false;

    private String errMsg;

    private Object buyObj;

    public PayEvent(boolean isSuccess) {
        isPaySuccess = isSuccess;
    }

    public PayEvent(boolean isSuccess, String errMsg) {
        this(isSuccess);
        this.errMsg = errMsg;
    }

    public PayEvent(boolean isSuccess, String errMsg, Object buyObj) {
        this(isSuccess, errMsg);
        this.buyObj = buyObj;
    }

    public boolean isSuccess() {
        return isPaySuccess;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public Object getBuyObj() {
        return buyObj;
    }
}
