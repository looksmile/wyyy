package com.tgkj.wyyy.events;

/**
 * Created by lenovo on 2015/9/22.
 */
public class WXPayEvent {
    private String errMsg;
    private boolean isSuccess;

    public WXPayEvent(boolean isSuccess, String errMsg) {
        this.isSuccess = isSuccess;
        this.errMsg = errMsg;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
