package com.tgkj.wyyy.events;

/**
 * Created by Administrator on 2016/3/3.
 */
public class FamousePageEvent {
    public boolean isSuccess = false;
    public String errMsg;
    public FamousePageEvent(boolean isSuccess, String errMsg){
        this.isSuccess = isSuccess;
        this.errMsg = errMsg;
    }
}
