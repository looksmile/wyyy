package com.tgkj.wyyy.events;

import com.tgkj.wyyy.info.Vip;

/**
 * 下单事件
 * Created by Administrator on 2015/11/18.
 */
public class PlaceOrderEvent {
    public boolean isSuccess;
    public String errMsg;
    public Vip buyObj;
    public int payType;

    public PlaceOrderEvent(boolean isSuccess, Vip buyObj, int payType, String errMsg) {
        this.isSuccess = isSuccess;
        this.buyObj = buyObj;
        this.payType = payType;
        this.errMsg = errMsg;
    }
}
