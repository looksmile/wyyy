package com.tgkj.wyyy.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015/12/18.
 */
public class ShowAdAdapter extends PagerAdapter {
    private ArrayList<View> listViews;

    private int size;
    public ShowAdAdapter(ArrayList<View> listViews){
        this.listViews = listViews;
        size = listViews == null ? 0 : listViews.size();
    }
    @Override
    public int getCount() {
        return size;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(listViews.get(position), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return listViews.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(listViews.get(position));
    }

}
