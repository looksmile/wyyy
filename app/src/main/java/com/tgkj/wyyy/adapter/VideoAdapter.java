package com.tgkj.wyyy.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.activity.BaseActivity;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.views.VideoImageView;

import java.util.List;

/**
 * Created by Administrator on 2015/10/12.
 */
public class VideoAdapter extends BaseAdapter {
    private List<VideoInfo> list;
    private BaseActivity context;
    private LayoutInflater inflater;
    private Holder holder;
    private boolean free;
    public VideoAdapter(Context context, List<VideoInfo> list){
        this.context= (BaseActivity) context;
        this.list=list;
        this.inflater = LayoutInflater.from(this.context);
        this.free = false;
    }

    public VideoAdapter(Context context, List<VideoInfo> list, boolean free){
        this.context= (BaseActivity) context;
        this.list=list;
        this.inflater = LayoutInflater.from(this.context);
        this.free = free;
    }
    @Override
    public int getCount() {
        if(list!=null){
            return list.size();
        }
        return 0;
    }

    @Override
    public VideoInfo getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.video_item,parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (Holder)convertView.getTag();
        }
        holder.setData(list.get(position));
        holder.movieImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DataCenter.getInstance().userInfo.VipLevel > 0){
                    context.playVideo(list.get(position).Address, true);
                    return;
                }
                if(!free){
                    String title = "需要开通VIP才能观看";
                    context.showOverDialog(title);
                }else if(DataCenter.getInstance().rest_play > 0) {
                    context.playVideo(list.get(position).Address, false);
                }else{
                    String title = "您的免费播放次数已用完";
                    context.showOverDialog(title);
                }
            }
        });
        return convertView;
    }
    class Holder{
        public TextView movieName;
//        public TextView movieLooker;
        public VideoImageView movieImage;
        public Holder(View view){
            movieName = (TextView)view.findViewById(R.id.movieName);
//            movieLooker = (TextView)view.findViewById(R.id.movieLooker);
            movieImage = (VideoImageView)view.findViewById(R.id.movieImage);
        }
        public void setData(VideoInfo vinfo){
            movieName.setText(vinfo.Name);
//            movieLooker.setText(vinfo.KeyWords);
            ImageLoader.getInstance().displayImage(Constant.bitmap_url+vinfo.Image,movieImage);
        }

    }

}
