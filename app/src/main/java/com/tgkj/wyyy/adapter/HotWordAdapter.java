package com.tgkj.wyyy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.tgkj.wyyy.R;
import com.tgkj.wyyy.info.HotItemInfo;

import java.util.List;

/**
 * Created by Administrator on 2015/10/12.
 */
public class HotWordAdapter extends BaseAdapter {
    private List<HotItemInfo> list;
    private Context context;
    private LayoutInflater inflater;
    private Holder holder;
    public interface OnWordClickListener{
        void onClick(HotItemInfo info);
    }
    private OnWordClickListener mListener;
    public HotWordAdapter(Context context, List<HotItemInfo> list, OnWordClickListener listener){
        this.context=context;
        this.list=list;
        this.inflater = LayoutInflater.from(this.context);
        this.mListener=listener;
    }
    @Override
    public int getCount() {
        if(list!=null){
            return list.size();
        }
        return 0;
    }

    @Override
    public HotItemInfo getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.search_hot_word_layout,parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (Holder)convertView.getTag();
        }
        holder.updateView(list.get(position));
        return convertView;
    }
    class Holder{
        public Button txt;
        public HotItemInfo info;
        public Holder(View view){
            txt = (Button) view.findViewById(R.id.txt);
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onClick(info);
                }
            });
        }
        public void updateView(HotItemInfo info){
            this.info=info;
            this.txt.setText(info.Name);
        }

    }

}
