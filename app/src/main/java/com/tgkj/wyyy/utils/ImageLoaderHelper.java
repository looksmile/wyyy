package com.tgkj.wyyy.utils;

import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by lenovo on 2015/8/26.
 */
public class ImageLoaderHelper {

    public static ImageLoaderConfiguration buildConfig(Context context) {
        //File cacheDir = StorageUtils.getOwnCacheDirectory(MyApplication.getMyApplication(), "imageloader/Cache");
        return new ImageLoaderConfiguration.Builder(context)
                .diskCacheSize(50 * 1024 * 1204)
                .diskCacheFileCount(200)
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder().cacheOnDisk(true).cacheInMemory(true).build())
                .writeDebugLogs()
                .build();
    }

    /**
     * 用于home activity 图片加载
     *
     * @return
     */
    public static DisplayImageOptions buildHomeDisplayOptions() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

}
