package com.tgkj.wyyy.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
	
	public static final int FORMAT_SECONDS = 0;
	public static final int FORMAT_MINUTE = 1;
	public static final int FORMAT_HOUR = 2;
	
	private static DateTimeUtil dtUtil;
	private DateTimeUtil(){
		
	}
	
	public static DateTimeUtil getInstance(){
		if(dtUtil == null){
			synchronized(DateTimeUtil.class){
				if(dtUtil == null){
					dtUtil = new DateTimeUtil();
				}
			}
		}
		return dtUtil;
	}
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public long getCompareValue(Calendar src,Calendar dist,int type){
		if(src == null || dist == null){
			throw new IllegalArgumentException();
		}
		long result = 0;
		long i = dist.getTimeInMillis() - src.getTimeInMillis();//得到的毫秒书
		switch(type){
			case FORMAT_HOUR:
				result = i/(1000*60*60);
				break;
			case FORMAT_MINUTE:
				result = i / (1000 * 60);
				break;
			case FORMAT_SECONDS:
				result = i/1000;
				break;
			default:
				result = i;
				break;
		}
		return result;
	}
	
	
	public long getCompareValue(String src,String dist,int type){
		if(src == null || dist == null){
			throw new IllegalArgumentException();
		}
		long result = 0;
		Calendar cal_src = Calendar.getInstance();
		Calendar cal_dist = Calendar.getInstance();
		try {
			cal_dist.setTime(sdf.parse(dist));
			cal_src.setTime(sdf.parse(src));
			long i = cal_dist.getTimeInMillis() - cal_src.getTimeInMillis();//得到的毫秒书
			switch(type){
				case FORMAT_HOUR:
					result = i/(1000*60*60);
					break;
				case FORMAT_MINUTE:
					result = i / (1000 * 60);//分数
					break;
				case FORMAT_SECONDS:
					result = i/1000;
					break;
				default:
					result = i;
					break;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public long getCompareValue(Date src,Date dist,int type){
		if(src == null || dist == null){
			throw new IllegalArgumentException();
		}
		long result = 0;
		Calendar cal_src = Calendar.getInstance();
		Calendar cal_dist = Calendar.getInstance();
		cal_dist.setTime(dist);
		cal_src.setTime(src);
		long i = cal_dist.getTimeInMillis() - cal_src.getTimeInMillis();//得到的毫秒书
		switch(type){
			case FORMAT_HOUR:
				result = i/(1000*60*60);
				break;
			case FORMAT_MINUTE:
				result = i / (1000 * 60);//分数
				break;
			case FORMAT_SECONDS:
				result = i/1000;
				break;
			default:
				result = i;
				break;
		}
		return result;
	}
	
	public static String getCureentTime(){
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return s.format(date);
	}
	
	public static String getTime(long timesampe){
		Date date = new Date(timesampe);
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return s.format(date);
	}
}
