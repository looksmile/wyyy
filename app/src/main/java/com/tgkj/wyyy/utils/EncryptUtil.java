package com.tgkj.wyyy.utils;

import java.io.IOException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

public class EncryptUtil {
	/**
     * DES 加密
     *
     * @param content
     * @param password
     * @return
     * @throws Exception
     */
    public static String desEncrypt(String content, String password) throws Exception {
        SecureRandom sr = new SecureRandom();
        byte rawKeyData[] = password.getBytes();
        DESKeySpec dks = new DESKeySpec(rawKeyData);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey key = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.ENCRYPT_MODE, key, sr);
        byte data[] = content.getBytes();
        byte encryptedData[] = cipher.doFinal(data);
        return base64Encode(encryptedData);

    }

    /**
     * DES 解密
     *
     * @param encryptText
     * @param password
     * @return
     * @throws Exception
     */
    public static String desDecrypt(String encryptText, String password) throws Exception {
        SecureRandom sr = new SecureRandom();
        byte rawKeyData[] = password.getBytes();
        DESKeySpec dks = new DESKeySpec(rawKeyData);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey key = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.DECRYPT_MODE, key, sr);
        byte encryptedData[] = base64Decode(encryptText);
        byte decryptedData[] = cipher.doFinal(encryptedData);
        return new String(decryptedData);
    }

    public static String base64Encode(byte[] s) {
        if (s == null)
            return null;
        BASE64Encoder b = new BASE64Encoder();
        return b.encode(s);
    }

    public static byte[] base64Decode(String s) throws IOException {
        if (s == null)
            return null;
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] b = decoder.decodeBuffer(s);
        return b;
    }
}
