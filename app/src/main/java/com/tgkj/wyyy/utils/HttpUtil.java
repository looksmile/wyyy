package com.tgkj.wyyy.utils;

import android.util.Log;

import com.tgkj.wyyy.Constant;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpUtil {
	public static String post(String url, JSONObject postData) throws Exception {
        String data = "";
        HttpPost httpPost = null;
        HttpClient httpClient = null;

            String timeStamp = String.valueOf(new Date().getTime());
            String source;
            if(postData.has("OrderID")){
                source = postData.getString("OrderID") + timeStamp + Constant.md5;
            }else{
                source = timeStamp + Constant.md5;
            }
            String Key = MD5Util.toMD5(source);
            postData.put("Time", timeStamp);
            postData.put("Key", Key);
            HashMap<String,String> params = new HashMap<String,String>();
            String jsonContent = postData.toString();
            String encryptContent = EncryptUtil.desEncrypt(jsonContent, Constant.des);
            params.put("Content", encryptContent);

            httpClient = new DefaultHttpClient();
            httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
            httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);
            httpPost = new HttpPost(url);
            List<NameValuePair> httpParams = new ArrayList<NameValuePair>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                httpParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
            }
            httpPost.setHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(httpParams, "UTF-8");
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String encryptResult = EntityUtils.toString(response.getEntity());
                data = EncryptUtil.desDecrypt(encryptResult, Constant.des);
            }

       return data;

	}

    public static String get(String url, JSONObject param) throws Exception {

        String data = "";
            if(param != null){
                String jsonContent = param.toString();
                String encryptContent = EncryptUtil.desEncrypt(jsonContent, Constant.des);
                url += "?" + "Content=" + URLEncoder.encode(encryptContent, "UTF-8");
            }
            HttpGet getMethod = new HttpGet(url);
            DefaultHttpClient client = new DefaultHttpClient(new BasicHttpParams());
            client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);
            HttpResponse httpResponse = client.execute(getMethod);
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String encryptResult = EntityUtils.toString(httpResponse.getEntity());
                data = EncryptUtil.desDecrypt(encryptResult, Constant.des);
            }

        return data;
    }

}
