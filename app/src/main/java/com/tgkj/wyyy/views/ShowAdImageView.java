package com.tgkj.wyyy.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ShowAdImageView extends ImageView {
	public int index=0;
	public ShowAdImageView(Context context) {
		super(context);
	}

	public ShowAdImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ShowAdImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	@Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int currentWidth=(int)(getResources().getDisplayMetrics().widthPixels);
		int currentHeight=(currentWidth*202/360);
		setMeasuredDimension(currentWidth, currentHeight);
	}
}
