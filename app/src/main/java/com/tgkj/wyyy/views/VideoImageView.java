package com.tgkj.wyyy.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Administrator on 2015/9/14.
 */
public class VideoImageView extends ImageView {
    public VideoImageView(Context context) {
        super(context);
    }

    public VideoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int currentWidth=getResources().getDisplayMetrics().widthPixels*109/360;
		int currentHeight=(int)(currentWidth*162/109);
        setMeasuredDimension(currentWidth,currentHeight);
//        super.onMeasure(currentWidth, currentHeight);
    }
}
