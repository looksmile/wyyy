package com.tgkj.wyyy.jobs;

import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.events.PlaceOrderEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.Vip;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/3/9.
 */
public class PlaceOrderJob extends Job {

    private Vip vip;
    private String orderId;
    private int payType;
    private static final int VIP = 3;


    public PlaceOrderJob(Vip vip, String orderId, int payType){
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("place_order"));
        this.vip = vip;
        this.orderId = orderId;
        this.payType = payType;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "V2-3-1/PayOrder.aspx");
        JSONObject jo = new JSONObject();
        jo.put("RequestType", "PayOrder");
        jo.put("OrderID", orderId);
        jo.put("UID", DataCenter.getInstance().userInfo.UID);
        jo.put("Amount", vip.getPrice());
        jo.put("ProductID", Constant.ProductID);
        jo.put("CanalID", Constant.ChannelID);
        jo.put("PayType", payType);
        jo.put("ItemType", VIP);
        jo.put("VipID", vip.getVipID());

        String result = HttpUtil.post(url, jo);
        Log.d("zq", "result is " + result);

        JSONObject json = new JSONObject(result);
        if (json.getInt("Code") == 1) {
            EventBus.getDefault().post(new PlaceOrderEvent(true, vip, payType, null));
        } else {
            EventBus.getDefault().post(new PlaceOrderEvent(false, vip, payType, "接口异常"));
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }
}
