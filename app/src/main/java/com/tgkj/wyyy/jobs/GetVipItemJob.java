package com.tgkj.wyyy.jobs;


import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.events.GetVipItemEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.Vip;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * 获取Vip详情job
 * Created by Administrator on 2015/12/23.
 */
public class GetVipItemJob extends Job {

    public GetVipItemJob() {
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("GetVipItemJob"));
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "V2-4-2/VipConfigList.aspx");
        JSONObject params = new JSONObject();
        params.put("VersionID", 2);
        params.put("UID", DataCenter.getInstance().userInfo.UID);
        String result = HttpUtil.post(url, params);
        Log.d("zq", "kkkkk is " + result);
        parse(result);
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new GetVipItemEvent(false, throwable.getMessage()));
        return false;
    }

    private void parse(String json) throws JSONException {

        JSONObject jo = new JSONObject(json);
        if (jo.getInt("Code") == 1) {
            JSONArray vipJa = jo.getJSONArray("VipConfigList");
            DataCenter.getInstance().vips.clear();
            //解析vip会员项
            for (int i = 0; i < vipJa.length(); i++) {
                JSONObject vipJo = vipJa.getJSONObject(i);
                Vip v = new Vip();
                v.setPrice(vipJo.getInt("Price"));
                v.setVipName(vipJo.getString("Name"));
                v.setVipID(vipJo.getInt("VipID"));
                v.setExpire(vipJo.getInt("EndTime"));
                v.setDesc(vipJo.getString("Describle"));
                DataCenter.getInstance().vips.add(v);
            }

            EventBus.getDefault().post(new GetVipItemEvent(true, null));
        } else {
            EventBus.getDefault().post(new GetVipItemEvent(false, "请求异常"));
        }

    }
}
