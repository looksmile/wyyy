package com.tgkj.wyyy.jobs;

import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.events.SearchEvent;
import com.tgkj.wyyy.info.BannerInfo;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/2/29.
 */
public class GetSearchVideoJob extends Job{


    private String keyWord;

    public GetSearchVideoJob(String keyWord) {
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("searchJob"));
        this.keyWord = keyWord;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "MidnightCinema/VideoSearchList.aspx");
        JSONObject jo = new JSONObject();
        jo.put("KeyWords", keyWord);
        String result = HttpUtil.post(url, jo);
        parseData(new JSONObject(result));
        EventBus.getDefault().post(new SearchEvent(true, null));
    }


    private void parseData(JSONObject json) throws JSONException {
        DataCenter.getInstance().searchList.clear();
        JSONArray commandList = json.getJSONArray("VideoSearchList");
        for (int i = 0; i < commandList.length(); i++) {
            JSONObject obj = commandList.getJSONObject(i);
            VideoInfo movie = new VideoInfo();
            movie.ID = obj.optInt("ID");
            movie.Name = obj.optString("Name");
            movie.Image = obj.optString("Image");
            movie.Sort = obj.optInt("Sort");
            movie.Address = obj.optString("Address");
            movie.PTime = obj.optInt("PTime");
            movie.Column = obj.optInt("Column");
            movie.Classify = obj.optInt("Classify");
            movie.KeyWords = obj.optString("KeyWords");
            movie.DramaID = obj.optInt("DramaID");
            DataCenter.getInstance().searchList.add(movie);
        }

    }
    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new SearchEvent(false, throwable.getMessage()));
        return false;
    }
}
