package com.tgkj.wyyy.jobs;

import android.content.SharedPreferences;
import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.events.FamousePageEvent;
import com.tgkj.wyyy.events.LoginEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.utils.HttpUtil;
import com.tgkj.wyyy.utils.MD5Util;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/3/8.
 */
public class LoginJob extends Job {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;


    public LoginJob(){
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("loginJob"));
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "MidnightCinema/MidnightUserLogin.aspx");
        JSONObject jo = new JSONObject();

        String deviceId = MyApplication.getInstance().getDevice();

        jo.put("PassWord", MD5Util.toMD5("123456" + Constant.md5));
        jo.put("DeviceID", deviceId);
        jo.put("CanalID", Constant.ChannelID);
        jo.put("ProductID", Constant.ProductID);

        String result = HttpUtil.post(url, jo);
        Log.d("zq", "user info is " + result);

        parseData(new JSONObject(result));
        EventBus.getDefault().post(new LoginEvent(true, null));
    }

    @Override
    protected void onCancel() {

    }

    private void parseData(JSONObject json) throws JSONException{
        int isReg = json.getInt("IsReg");
        if(isReg == 0){
            mSharedPreferences = MyApplication.getInstance().getSharedPreferences("userInfo", 0);
            mEditor = mSharedPreferences.edit();
            mEditor.putInt("rest_play", 5);
            mEditor.commit();
            DataCenter.getInstance().rest_play = 5;
        }
        DataCenter.getInstance().userInfo.UID = json.getString("UID");
        DataCenter.getInstance().userInfo.VipLevel = json.getInt("Vip");
        DataCenter.getInstance().userInfo.VipEndTime = json.getString("VipEndTime");
    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new LoginEvent(false, throwable.getMessage()));
        return false;
    }
}
