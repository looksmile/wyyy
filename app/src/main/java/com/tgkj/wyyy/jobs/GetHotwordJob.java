package com.tgkj.wyyy.jobs;

import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.events.HotwordEvent;
import com.tgkj.wyyy.info.BannerInfo;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.HotItemInfo;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/3/7.
 */
public class GetHotwordJob extends Job {

    public GetHotwordJob() {
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("getHotJob"));
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "MidnightCinema/HotWordsList.aspx");
        String result = HttpUtil.post(url, new JSONObject());
        parseData(new JSONObject(result));
        EventBus.getDefault().post(new HotwordEvent(true, null));
    }

    private void parseData(JSONObject json) throws JSONException{
        DataCenter.getInstance().hotList.clear();
        //取得头部Banner信息
        JSONArray hotArrayList = json.getJSONArray("HotWordsList");
        for (int i = 0; i < hotArrayList.length(); i++) {
            HotItemInfo hot = new HotItemInfo(hotArrayList.getJSONObject(i));
            DataCenter.getInstance().hotList.add(hot);
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new HotwordEvent(false, throwable.getMessage()));
        return false;
    }
}
