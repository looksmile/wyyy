package com.tgkj.wyyy.jobs;

import android.provider.ContactsContract;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.events.HomeboyPageEvent;
import com.tgkj.wyyy.info.BannerInfo;
import com.tgkj.wyyy.info.ClassifyInfo;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/2/29.
 */
public class GetHomeboyVideoJob extends Job{

    public GetHomeboyVideoJob() {
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("homeboyJob"));
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "MidnightCinema/VideoListFour.aspx");
        String result = HttpUtil.post(url, new JSONObject());
        parseData(new JSONObject(result));
        EventBus.getDefault().post(new HomeboyPageEvent(true, null));
    }
    private void parseData(JSONObject json) throws JSONException {
        DataCenter.getInstance().homeboyBannerList.clear();
        DataCenter.getInstance().homeboyVideoList.clear();
        DataCenter.getInstance().homeboyTabList.clear();
        //取顶部Tab标签栏
        JSONArray joClassifyList = json.getJSONArray("ClassifyList");
        for (int i = 0; i < joClassifyList.length(); i++) {
            ClassifyInfo classifyInfo = new ClassifyInfo();
            JSONObject classifyjo = joClassifyList.getJSONObject(i);
            classifyInfo.ID = classifyjo.optInt("ID");
            classifyInfo.Name = classifyjo.optString("Name");
            classifyInfo.Sort = classifyjo.optInt("Sort");

            DataCenter.getInstance().homeboyTabList.add(classifyInfo);
        }
        Comparator comp = new Comparator() {
            public int compare(Object o1, Object o2) {
                ClassifyInfo p1 = (ClassifyInfo) o1;
                ClassifyInfo p2 = (ClassifyInfo) o2;
                if (p1.Sort < p2.Sort)
                    return 1;
                else if (p1.Sort == p2.Sort)
                    return 0;
                else if (p1.Sort > p2.Sort)
                    return -1;
                return 0;
            }
        };
        Collections.sort(DataCenter.getInstance().homeboyTabList, comp);
        //取得头部Banner信息
        JSONArray joBannerList = json.getJSONArray("BannerList");
        for (int i = 0; i < joBannerList.length(); i++) {
            BannerInfo banner = new BannerInfo();
            JSONObject bannerJo = joBannerList.getJSONObject(i);
            banner.Image = bannerJo.optString("Image");
            banner.ID = bannerJo.optInt("ID");
            banner.Address = bannerJo.optString("Address");
            banner.Position = bannerJo.optInt("Position");
            banner.Sort = bannerJo.optInt("Sort");

            DataCenter.getInstance().homeboyBannerList.add(banner);
        }
        //取得首页视频列表
        JSONArray commandList = json.getJSONArray("VideoList");
        for (int i = 0; i < commandList.length(); i++) {
            JSONObject obj = commandList.getJSONObject(i);
            VideoInfo movie = new VideoInfo();
            movie.ID = obj.optInt("ID");
            movie.Name = obj.optString("Name");
            movie.Image = obj.optString("Image");
            movie.Sort = obj.optInt("Sort");
            movie.Address = obj.optString("Address");
            movie.PTime = obj.optInt("PTime");
            movie.Column = obj.optInt("Column");
            movie.Classify = obj.optInt("Classify");
            movie.KeyWords = obj.optString("KeyWords");
            movie.DramaID = obj.optInt("DramaID");
            DataCenter.getInstance().homeboyVideoList.add(movie);
        }

    }
    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new HomeboyPageEvent(false, throwable.getMessage()));
        return false;
    }
}
