package com.tgkj.wyyy.jobs;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.events.FamousePageEvent;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.info.BannerInfo;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/2/29.
 */
public class GetFamouseVideoJob extends Job{

    public GetFamouseVideoJob() {
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("famouseJob"));
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "MidnightCinema/VideoListThree.aspx");
        String result = HttpUtil.post(url, new JSONObject());
        parseData(new JSONObject(result));
        EventBus.getDefault().post(new FamousePageEvent(true, null));
    }
    private void parseData(JSONObject json) throws JSONException {
        DataCenter.getInstance().famouseBannerList.clear();
        DataCenter.getInstance().famouseVideoList.clear();
        //取得头部Banner信息
        JSONArray joBannerList = json.getJSONArray("BannerList");
        for (int i = 0; i < joBannerList.length(); i++) {
            BannerInfo banner = new BannerInfo();
            JSONObject bannerJo = joBannerList.getJSONObject(i);
            banner.Image = bannerJo.optString("Image");
            banner.ID = bannerJo.optInt("ID");
            banner.Address = bannerJo.optString("Address");
            banner.Position = bannerJo.optInt("Position");
            banner.Sort = bannerJo.optInt("Sort");

            DataCenter.getInstance().famouseBannerList.add(banner);
        }
        //取得首页视频列表
        JSONArray commandList = json.getJSONArray("VideoList");
        for (int i = 0; i < commandList.length(); i++) {
            JSONObject obj = commandList.getJSONObject(i);
            VideoInfo movie = new VideoInfo();
            movie.ID = obj.optInt("ID");
            movie.Name = obj.optString("Name");
            movie.Image = obj.optString("Image");
            movie.Sort = obj.optInt("Sort");
            movie.Address = obj.optString("Address");
            movie.PTime = obj.optInt("PTime");
            movie.Column = obj.optInt("Column");
            movie.Classify = obj.optInt("Classify");
            movie.KeyWords = obj.optString("KeyWords");
            movie.DramaID = obj.optInt("DramaID");
            DataCenter.getInstance().famouseVideoList.add(movie);
        }

    }
    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new FamousePageEvent(false, throwable.getMessage()));
        return false;
    }
}
