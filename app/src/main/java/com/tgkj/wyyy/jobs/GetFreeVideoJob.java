package com.tgkj.wyyy.jobs;

import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.events.FreeVideoEvent;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.utils.HttpUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by apple on 16/3/9.
 */
public class GetFreeVideoJob extends Job {

    public GetFreeVideoJob(){
        super(new Params(JobPriority.HIGH).requireNetwork().groupBy("freeJob"));
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        String url = String.format("%s%s", Constant.ServerUrl, "MidnightCinema/VideoListForVip.aspx");
        String result = HttpUtil.post(url, new JSONObject());

        parseData(new JSONObject(result));
        EventBus.getDefault().post(new FreeVideoEvent(true, null));
    }

    private void parseData(JSONObject json) throws JSONException{
        DataCenter.getInstance().vipVideoList.clear();
        JSONArray commandList = json.getJSONArray("VideoList");
        for (int i = 0; i < commandList.length(); i++) {
            JSONObject obj = commandList.getJSONObject(i);
            VideoInfo movie = new VideoInfo();
            movie.ID = obj.optInt("ID");
            movie.Name = obj.optString("Name");
            movie.Image = obj.optString("Image");
            movie.Sort = obj.optInt("Sort");
            movie.Address = obj.optString("Address");
            movie.PTime = obj.optInt("PTime");
            movie.Column = obj.optInt("Column");
            movie.Classify = obj.optInt("Classify");
            movie.KeyWords = obj.optString("KeyWords");
            movie.DramaID = obj.optInt("DramaID");
            DataCenter.getInstance().vipVideoList.add(movie);
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new FreeVideoEvent(false, null));
        return false;
    }
}
