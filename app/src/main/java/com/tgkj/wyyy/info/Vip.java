package com.tgkj.wyyy.info;

/**
 * Vip类
 * Created by lenovo on 2015/9/7.
 */
public class Vip {
    private String vipName;
    private String desc;
    private int price;
    private int vipID;
    private int gdTicketCount;//购买后给的普通影券数量
    private int hdTicketCount;//购买后给的高清影券数量
    private int loginGivePointsTimes;//登录送的积分数倍数
    private int givePoints;//购买后一次性送的积分数
    private int rate;//折扣率 1-10，10表示不打折
    private int expire;//过期天数
    private int diamondCount;//钻石数

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getVipID() {
        return vipID;
    }

    public void setVipID(int vipID) {
        this.vipID = vipID;
    }

    public int getGdTicketCount() {
        return gdTicketCount;
    }

    public void setGdTicketCount(int gdTicketCount) {
        this.gdTicketCount = gdTicketCount;
    }

    public int getHdTicketCount() {
        return hdTicketCount;
    }

    public void setHdTicketCount(int hdTicketCount) {
        this.hdTicketCount = hdTicketCount;
    }

    public int getLoginGivePointsTimes() {
        return loginGivePointsTimes;
    }

    public void setLoginGivePointsTimes(int loginGivePointsTimes) {
        this.loginGivePointsTimes = loginGivePointsTimes;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public int getGivePoints() {
        return givePoints;
    }

    public void setGivePoints(int givePoints) {
        this.givePoints = givePoints;
    }

    public int getDiamondCount() {
        return diamondCount;
    }

    public void setDiamondCount(int diamondCount) {
        this.diamondCount = diamondCount;
    }
}
