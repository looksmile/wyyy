package com.tgkj.wyyy.info;

import java.io.Serializable;


public class UserInfo implements Serializable {
    public String UID = "";//用户ID
    public String NickName = "";//昵称
    public int VipLevel = 0; //vip ID,(0表示非vip，1表示vip1，以此类推，100表示自尊vip）
    public String VipName = "";//vip名称
    public String VipEndTime = "";//vip截至时间，例如：5，表示5天后到期
    public String RegTime = "";//注册时间
    public int Points = 0;//用户积分余额
    public String VideoID = "";//已经买过的视频id，逗号分隔
    public int Daily = 0;//每日奖励标记
    public int ticketCount = 0;//普通观影券数量
    public int hdTicketCount = 0;//高清观影券数量
    public int loginAwardPoints = 0;//登录奖励的积分数
    public int vipRate = 10; //打的折数，1-10，10表示不打折
    public int diamondCount = 0;//钻石数

    //非登录注册返回的信息
    public boolean isVisitor = true;//默认为游客
    public String Account = "";//账户名
    public boolean isSign = false;
    public int signDay = 0;
    public String password = "";
}
