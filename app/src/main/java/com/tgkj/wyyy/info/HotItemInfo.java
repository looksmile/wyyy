package com.tgkj.wyyy.info;

import org.json.JSONObject;

/**
 * Created by Administrator on 2015/10/13.
 */
public class HotItemInfo {
    public int ID;
    public String Name;
    public int Sort;
    public HotItemInfo(JSONObject jo){
        ID=jo.optInt("ID");
        Name=jo.optString("Name");
        Sort=jo.optInt("Sort");
    }
}
