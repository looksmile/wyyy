package com.tgkj.wyyy.info;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/3.
 */
public class DataCenter {
    private static DataCenter _instance;
    public static DataCenter getInstance(){
        if(_instance==null){
            _instance=new DataCenter();
        }
        return _instance;
    }
    //女忧头部位广告
    public List<BannerInfo> girlBannerList = new ArrayList<BannerInfo>();
    //女忧视频列表
    public List<VideoInfo> girlVideoList = new ArrayList<VideoInfo>();
    //综艺头部位广告
    public List<BannerInfo> varietyBannerList = new ArrayList<BannerInfo>();
    //综艺视频列表
    public List<VideoInfo> varietyVideoList = new ArrayList<VideoInfo>();
    //大片头部位广告
    public List<BannerInfo> famouseBannerList = new ArrayList<BannerInfo>();
    //大片视频列表
    public List<VideoInfo> famouseVideoList = new ArrayList<VideoInfo>();
    //宅男头部Tab标签
    public List<ClassifyInfo> homeboyTabList = new ArrayList<ClassifyInfo>();
    //宅男头部位广告
    public List<BannerInfo> homeboyBannerList = new ArrayList<BannerInfo>();
    //宅男视频列表
    public List<VideoInfo> homeboyVideoList = new ArrayList<VideoInfo>();
    //搜索热词列表
    public List<HotItemInfo> hotList = new ArrayList<HotItemInfo>();
    //搜索视频列表
    public List<VideoInfo> searchList = new ArrayList<VideoInfo>();
    //vip试看视频列表
    public List<VideoInfo> vipVideoList = new ArrayList<VideoInfo>();
    //剩余观影次数
    public int rest_play;
    //用户信息
    public UserInfo userInfo = new UserInfo();
    //vip详情
    public List<Vip> vips = new ArrayList<Vip>();

}
