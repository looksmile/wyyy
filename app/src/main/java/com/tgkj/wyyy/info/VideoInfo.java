package com.tgkj.wyyy.info;

/**
 * Created by Administrator on 2016/3/2.
 */
public class VideoInfo {
    public int ID;
    public String Name;
    public int Sort;
    public String Image;//封面图
    public String Address;//播放地址
    public int PTime;//播放时长（秒）
    public int Column;//所属栏目
    public int Classify;//所属分类
    public String KeyWords;//关键词搜索
    public int DramaID;//所属剧集
}
