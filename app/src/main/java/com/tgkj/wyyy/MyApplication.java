package com.tgkj.wyyy;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.log.CustomLogger;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.utils.ImageLoaderHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/12/15.
 */
public class MyApplication extends Application {
    private List<Activity> mActivities = new ArrayList<Activity>();
    private static JobManager jobManager;
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initChannel();
        ImageLoader.getInstance().init(ImageLoaderHelper.buildConfig(this));
        configureJobManager();
    }

    public static MyApplication getInstance(){
        return instance;
    }

    private void configureJobManager() {
        Configuration configuration = new Configuration.Builder(this)
                .customLogger(new CustomLogger() {
                    private static final String TAG = "JOBS";
                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minute
                .build();
        jobManager = new JobManager(this, configuration);
    }

    public static JobManager getJobManager() {
        return jobManager;
    }
    public void addActivity(Activity activity) {
        mActivities.add(activity);
    }
    public void removeActivity(Activity activity) {
        if(mActivities.contains(activity)){
            mActivities.remove(activity);
        }
    }

    private void initChannel(){
        ApplicationInfo appInfo = null;
        try {
            appInfo = this.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Bundle bundle = appInfo.metaData;
        Constant.ChannelID = String.valueOf(bundle.getInt("UMENG_CHANNEL"));
        if ("0".equals(Constant.ChannelID)) {
            Constant.ChannelID = bundle.getString("UMENG_CHANNEL");
        }
    }

    public String getDevice(){
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

}
