package com.tgkj.wyyy.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.events.WXPayEvent;
import com.tgkj.wyyy.pay.wxpay.WXpayKeys;


import de.greenrobot.event.EventBus;

/**
 * 接收微信回调结果
 * Created by lenovo on 2015/9/21.
 */
public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wxpay_result);
        api = WXAPIFactory.createWXAPI(this, WXpayKeys.WXIN_APP_ID);
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {
        int type = baseResp.getType();
        if(type == ConstantsAPI.COMMAND_PAY_BY_WX) {
            String errMsg;
            switch (baseResp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    EventBus.getDefault().post(new WXPayEvent(true, null));
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    errMsg = String.format("%s(%d)", "用户取消", baseResp.errCode);
                    EventBus.getDefault().post(new WXPayEvent(false, errMsg));
                    break;
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    errMsg = String.format("%s(%d)", "授权被拒绝", baseResp.errCode);
                    EventBus.getDefault().post(new WXPayEvent(false, errMsg));
                    break;
                case BaseResp.ErrCode.ERR_COMM:
                    errMsg = String.format("%s(%d)", "微信登录已过期,请重新登录微信", baseResp.errCode);
                    EventBus.getDefault().post(new WXPayEvent(false, errMsg));
                    break;
                case BaseResp.ErrCode.ERR_UNSUPPORT:
                    errMsg = String.format("%s(%d)", "微信版本不支持", baseResp.errCode);
                    EventBus.getDefault().post(new WXPayEvent(false, errMsg));
                    break;
                case BaseResp.ErrCode.ERR_SENT_FAILED:
                    errMsg = String.format("%s(%d)", "支付发送失败，请稍候重试", baseResp.errCode);
                    EventBus.getDefault().post(new WXPayEvent(false, errMsg));
                    break;
                default:
                    EventBus.getDefault().post(new WXPayEvent(false, "未知错误"));
                    break;
            }
        }
        finish();

    }
}
