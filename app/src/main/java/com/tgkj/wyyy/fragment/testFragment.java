package com.tgkj.wyyy.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tgkj.wyyy.R;

/**
 * Created by Administrator on 2016/3/7.
 */
public class testFragment extends BaseFragment {
    private View myLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myLayout=inflater.inflate(R.layout.fragment_girl,container,false);
        return myLayout;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
