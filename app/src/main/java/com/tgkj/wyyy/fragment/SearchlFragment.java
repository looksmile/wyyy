package com.tgkj.wyyy.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.activity.PayActivity;
import com.tgkj.wyyy.adapter.HotWordAdapter;
import com.tgkj.wyyy.adapter.VideoAdapter;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.events.HotwordEvent;
import com.tgkj.wyyy.events.LoginEvent;
import com.tgkj.wyyy.events.SearchEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.HotItemInfo;
import com.tgkj.wyyy.jobs.GetHotwordJob;
import com.tgkj.wyyy.jobs.GetSearchVideoJob;
import com.tgkj.wyyy.views.ShowAdPager;
import com.tgkj.wyyy.views.XListView;

/**
 * Created by Administrator on 2016/2/29.
 */
public class SearchlFragment extends BaseFragment implements XListView.IXListViewListener {
    private View myLayout;
    private TextView titleName;
    private GridView hotWord;
    private GridView search;
    private HotWordAdapter hotWordAdapter;
    private VideoAdapter videoAdapter;
    private Button titleBtn;
    private EditText search_content;
    private Button btn_search;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myLayout=inflater.inflate(R.layout.fragment_search,container,false);
        init();
        return myLayout;

    }
    private void init(){
        titleName=(TextView)myLayout.findViewById(R.id.titleName);
        titleName.setText(R.string.main_btn1);
        hotWord = (GridView) myLayout.findViewById(R.id.hotGrid);
        search_content = (EditText) myLayout.findViewById(R.id.search_content);


        hotWord.setAdapter(hotWordAdapter = new HotWordAdapter(getActivity(), DataCenter.getInstance().hotList,
                new HotWordAdapter.OnWordClickListener() {
            @Override
            public void onClick(HotItemInfo info) {
                startLoading(getResources().getString(R.string.waitMsg));
                search_content.setText(info.Name);
                MyApplication.getJobManager().addJobInBackground(new GetSearchVideoJob(info.Name));
            }
        }));

        search = (GridView) myLayout.findViewById(R.id.search_result);
        search.setAdapter(videoAdapter = new VideoAdapter(getActivity(), DataCenter.getInstance().searchList));
        startLoading(getResources().getString(R.string.waitMsg));
        MyApplication.getJobManager().addJobInBackground(new GetHotwordJob());

        titleBtn = (Button) myLayout.findViewById(R.id.titleBtn);
        showVip();
        titleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DataCenter.getInstance().userInfo.VipLevel == 0) {
                    startActivity(new Intent(getActivity(), PayActivity.class));
                }
            }
        });

        btn_search = (Button) myLayout.findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = search_content.getText().toString();
                if (TextUtils.isEmpty(content)){
                    showToast("请输入搜索内容");
                }else{
                    startLoading(getResources().getString(R.string.waitMsg));
                    MyApplication.getJobManager().addJobInBackground(new GetSearchVideoJob(content));
                }
            }
        });
    }
    @Override
    public void onRefresh() {
        showVip();
    }

    @Override
    public void onLoadMore() {

    }

    public void onEventMainThread(HotwordEvent event) {
        stopLoading();
        if(event.isSuccess){
            hotWordAdapter.notifyDataSetChanged();
        }
        else{
            showToast(getResources().getString(R.string.httpError));
        }
    }

    public void onEventMainThread(SearchEvent event) {
        stopLoading();
        if(event.isSuccess){
            if(DataCenter.getInstance().searchList.size() == 0){
               showToast("没有找到对应内容");
            }
            videoAdapter.notifyDataSetChanged();
        }
        else{
            showToast(getResources().getString(R.string.httpError));
        }
    }

    public void onEventMainThread(LoginEvent event) {
        stopLoading();
        if(event.isSuccess){
            showVip();
        }
        else{
            showToast(getResources().getString(R.string.httpError));
        }
    }

    private void showVip(){
        if(DataCenter.getInstance().userInfo.VipLevel == 0)
            return;
        String end = DataCenter.getInstance().userInfo.VipEndTime;
        String txt = "会员到期时间";
        String end_show = String.format("%s\n%s",txt, end.substring(0, end.indexOf("T")));
        SpannableString string = new SpannableString(end_show);
        string.setSpan(new AbsoluteSizeSpan(10,true), 0, txt.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        titleBtn.setText(string);
        titleBtn.setBackgroundResource(R.drawable.btn_member_selector);
    }

}
