package com.tgkj.wyyy.fragment;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;


import com.tgkj.wyyy.activity.BaseActivity;
import com.tgkj.wyyy.dialog.LoadingFragmentDialog;

import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/11/16.
 */
public class BaseFragment extends Fragment {

    private LoadingFragmentDialog dialogFragment;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected void showToast(String msg){
        Activity activity = getActivity();
        if(activity != null){
            Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        stopLoading();
    }

    protected BaseActivity getParentActivity(){
        return (BaseActivity)getActivity();
    }

//    protected void startLoading(String msg){
//        if(dialogFragment == null){
//            dialogFragment = new LoadingFragmentDialog();
//            dialogFragment.setCancelable(false);
//        }
//        dialogFragment.setMsg(msg);
//        dialogFragment.show(getChildFragmentManager(), dialogFragment.getTag());
//    }
//
//    protected void stopLoading(){
//        if(dialogFragment != null && !dialogFragment.isRemoving()){
//            dialogFragment.dismiss();
//            dialogFragment = null;
//        }
//    }

    public void onEventMainThread(Object event){

    }
    protected void startLoading(String msg){
        if(dialogFragment == null){
            dialogFragment = new LoadingFragmentDialog();
            dialogFragment.setCancelable(false);
        }
        if(!TextUtils.isEmpty(msg)){
            dialogFragment.setMsg(msg);
        }
        if(!dialogFragment.isAdded()&&!dialogFragment.isVisible()){
            dialogFragment.show(getChildFragmentManager(), dialogFragment.getTag());
        }
    }

    protected void stopLoading(){
        if(dialogFragment != null && !dialogFragment.isRemoving()){
            dialogFragment.dismiss();
            dialogFragment = null;
        }
    }

}
