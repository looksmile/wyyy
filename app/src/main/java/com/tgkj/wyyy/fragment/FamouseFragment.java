package com.tgkj.wyyy.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.activity.BaseActivity;
import com.tgkj.wyyy.activity.PayActivity;
import com.tgkj.wyyy.adapter.ShowAdAdapter;
import com.tgkj.wyyy.adapter.VideoAdapter;
import com.tgkj.wyyy.events.FamousePageEvent;
import com.tgkj.wyyy.events.GirlPageEvent;
import com.tgkj.wyyy.events.LoginEvent;
import com.tgkj.wyyy.info.BannerInfo;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.jobs.GetFamouseVideoJob;
import com.tgkj.wyyy.jobs.GetGirlVideoJob;
import com.tgkj.wyyy.views.HeaderGridView;
import com.tgkj.wyyy.views.ShowAdImageView;
import com.tgkj.wyyy.views.ShowAdPager;
import com.tgkj.wyyy.views.XListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/2/29.
 */
public class FamouseFragment extends BaseFragment implements XListView.IXListViewListener {
    private View myLayout;
    //当前的位置
    private int location = 0;
    private TextView titleName;

    private List<VideoInfo> videoGridInfoList;
    private List<BannerInfo> banerInfoList;

    private VideoAdapter videoAdapter;

    private ShowAdPager showAdPager;
    private ShowAdAdapter showAdAdpter;

    private ArrayList<View> adList=new ArrayList<View>();
    private ImageButton leftBtn;
    private ImageButton rightBtn;
    private RelativeLayout btnLayout;
    private HeaderGridView headerGridView;
    private View showAdLayout;
    private Button titleBtn;
    private BaseActivity context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myLayout=inflater.inflate(R.layout.fragment_famouse,container,false);
        context = (BaseActivity) getActivity();
        init();
        reflashPage();
        return myLayout;

    }
    private void init(){
        titleName=(TextView)myLayout.findViewById(R.id.titleName);
        titleName.setText(R.string.main_btn4);
        videoGridInfoList=new ArrayList<VideoInfo>();
        banerInfoList=new ArrayList<BannerInfo>();
        initAdPager();
        headerGridView=(HeaderGridView)myLayout.findViewById(R.id.pull_refresh_grid);
        videoAdapter = new VideoAdapter(getParentActivity(),videoGridInfoList, true);
        headerGridView.addHeaderView(showAdLayout);
        headerGridView.setAdapter(videoAdapter);

        titleBtn = (Button) myLayout.findViewById(R.id.titleBtn);
        showVip();
        titleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DataCenter.getInstance().userInfo.VipLevel == 0)
                    startActivity(new Intent(getActivity(), PayActivity.class));
            }
        });


    }
    private void initAdPager(){
        showAdLayout = getParentActivity().getLayoutInflater().inflate(R.layout.show_ad_pager,null);
        showAdPager=(ShowAdPager)showAdLayout.findViewById(R.id.pagerFixed);
        showAdPager.setOnPageChangeListener(pageChangeListener);
        leftBtn=(ImageButton)showAdLayout.findViewById(R.id.leftBtn);
        rightBtn=(ImageButton)showAdLayout.findViewById(R.id.rightBtn);
        btnLayout=(RelativeLayout)showAdLayout.findViewById(R.id.btnLayout);

        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(location>0){
                    showAdPager.setCurrentItem(--location);
                }
            }
        });
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (location < DataCenter.getInstance().girlBannerList.size() - 1) {
                    showAdPager.setCurrentItem(++location);
                }
            }
        });
    }
    public void reflashPage(){
        startLoading(getResources().getString(R.string.waitMsg));
        showVip();
        MyApplication.getJobManager().addJobInBackground(new GetFamouseVideoJob());
    }

    private void reflashAdPager(){
        final List<BannerInfo> ary = DataCenter.getInstance().famouseBannerList;
        adList.clear();
        for(int i=0;i<ary.size();i++){
            ShowAdImageView iv = new ShowAdImageView(getActivity());
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            final int finalI = i;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(DataCenter.getInstance().userInfo.VipLevel > 0){
                        context.playVideo(ary.get(finalI).Address, true);
                    }else if(DataCenter.getInstance().rest_play > 0) {
                        context.playVideo(ary.get(finalI).Address, false);
                    }else{
                        String title = "您的免费播放次数已用完";
                        context.showOverDialog(title);
                    }
                }
            });
            adList.add(iv);
            ImageLoader.getInstance().displayImage(Constant.bitmap_url+ary.get(i).Image, iv);
        }
        showAdAdpter=new ShowAdAdapter(adList);
        showAdPager.setAdapter(showAdAdpter);
        showAdPager.setCurrentItem(0);
        showAdAdpter.notifyDataSetChanged();
        timeHandler.removeCallbacks(runnable);
        timeHandler.postDelayed(runnable, TIME);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)btnLayout.getLayoutParams();
        int w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        showAdLayout.measure(w, h);
        int height = showAdLayout.getMeasuredHeight();
        int width = showAdLayout.getMeasuredWidth();
        layoutParams.height=height;
        btnLayout.setLayoutParams(layoutParams);
        btnLayout.setVisibility(View.VISIBLE);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener()
    {

        public void onPageSelected(int position)
        {
            location = position;
        }

        public void onPageScrolled(int arg0, float arg1, int arg2)
        {

        }

        public void onPageScrollStateChanged(int arg0)
        {

        }
    };
    private int TIME = 5000;
    private Handler timeHandler=new Handler();
    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            // handler自带方法实现定时器
            try {
                timeHandler.postDelayed(this, TIME);
                if(location==showAdPager.getChildCount()-1){
                    location=0;
                }
                else{
                    location++;
                }
                showAdPager.setCurrentItem(location);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("exception...");
            }
        }
    };
    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timeHandler.removeCallbacks(runnable);
    }
    public void onEventMainThread(FamousePageEvent event) {
        stopLoading();
        if(event.isSuccess){
            reflashAdPager();
            videoGridInfoList.clear();
            videoGridInfoList.addAll(DataCenter.getInstance().famouseVideoList);
            videoAdapter.notifyDataSetChanged();

        }
        else{
            showToast(getResources().getString(R.string.httpError));
        }
    }

    public void onEventMainThread(LoginEvent event) {
        stopLoading();
        if(event.isSuccess){
            showVip();
        }
        else{
            showToast(getResources().getString(R.string.httpError));
        }
    }

    private void showVip(){
        if(DataCenter.getInstance().userInfo.VipLevel == 0)
            return;
        String end = DataCenter.getInstance().userInfo.VipEndTime;
        String txt = "会员到期时间";
        String end_show = String.format("%s\n%s",txt, end.substring(0, end.indexOf("T")));
        SpannableString string = new SpannableString(end_show);
        string.setSpan(new AbsoluteSizeSpan(10,true), 0, txt.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        titleBtn.setText(string);
        titleBtn.setBackgroundResource(R.drawable.btn_member_selector);
    }
}
