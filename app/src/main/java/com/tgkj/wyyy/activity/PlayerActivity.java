package com.tgkj.wyyy.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tgkj.wyyy.R;
import com.tgkj.wyyy.player.VideoPlayView;

/**
 * Created by apple on 16/3/7.
 */
public class PlayerActivity extends BaseActivity {

    private VideoPlayView player;
    private RelativeLayout loading_progress;
    private TextView rest_time;
    private Handler handler = new Handler();
    private int rest;
    private RelativeLayout time_layout;

    private String url;
    private boolean showAll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        url = getIntent().getStringExtra("URL");
        showAll = getIntent().getBooleanExtra("ALL", false);

        if(TextUtils.isEmpty(url)){
            finish();
        }
        init();
    }

    Runnable r = new Runnable() {
        @Override
        public void run() {

            rest = 30 - player.getCurrentPosition()/1000;
            if(rest >= 0) {
                rest_time.setText(String.format("剩余播放时间：%d秒", rest));
                handler.postDelayed(r, 1000);
            }else{
                player.pause();
                showVipDialog();
            }

        }
    };

    private void init(){
        loading_progress = (RelativeLayout) findViewById(R.id.course_surfaceview_layout_loadinglogo);
        player = (VideoPlayView) findViewById(R.id.video_player);
        rest_time = (TextView) findViewById(R.id.rest_time);
        time_layout = (RelativeLayout) findViewById(R.id.time_layout);
        if(showAll)
            time_layout.setVisibility(View.GONE);

        player.setVideoURI(Uri.parse(url));

        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                player.start();
                if(!showAll)
                    handler.post(r);
                loading_progress.setVisibility(View.GONE);
            }
        });

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                finish();
            }
        });
    }
}
