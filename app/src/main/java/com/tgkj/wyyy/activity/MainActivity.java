package com.tgkj.wyyy.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.fragment.FamouseFragment;
import com.tgkj.wyyy.fragment.GirlFragment;
import com.tgkj.wyyy.fragment.HomeboyFragment;
import com.tgkj.wyyy.fragment.SearchlFragment;
import com.tgkj.wyyy.fragment.VarietylFragment;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.jobs.LoginJob;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private FrameLayout fragmentLayout;
    private LinearLayout btn0;
    private LinearLayout btn1;
    private LinearLayout btn2;
    private LinearLayout btn3;
    private LinearLayout btn4;
    private ImageView img0;
    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private ImageView img4;
    private FragmentManager fragmentManager;
    private GirlFragment girlFragment = null;
    private FamouseFragment famouseFragment = null;
    private HomeboyFragment homeboyFragment = null;
    private SearchlFragment searchlFragment = null;
    private VarietylFragment varietylFragment = null;
    private int curTabIndex;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("test", "create");
		init();
        login();
//        startActivity(new Intent(MainActivity.this, PayActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    private void init(){
        initPlay();
        fragmentLayout=(FrameLayout)findViewById(R.id.fragmentLayout);
        fragmentManager=getSupportFragmentManager();
        btn0=(LinearLayout)findViewById(R.id.btn0);
        btn1=(LinearLayout)findViewById(R.id.btn1);
        btn2=(LinearLayout)findViewById(R.id.btn2);
        btn3=(LinearLayout)findViewById(R.id.btn3);
        btn4=(LinearLayout)findViewById(R.id.btn4);
        img0=(ImageView)findViewById(R.id.img0);
        img1=(ImageView)findViewById(R.id.img1);
        img2=(ImageView)findViewById(R.id.img2);
        img3=(ImageView)findViewById(R.id.img3);
        img4=(ImageView)findViewById(R.id.img4);
        initEvent();
        changeTab(Constant.GIRLVIEW);
        Log.d("test", "init");
    }
    private void initPlay(){
        sharedPreferences = getSharedPreferences("userInfo", 0);
        DataCenter.getInstance().rest_play = sharedPreferences.getInt("rest_play",0);
        Log.d("zq", "rettst us " + DataCenter.getInstance().rest_play);
    }
    private void initEvent(){
        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
    }
    private void login(){
        MyApplication.getJobManager().addJobInBackground(new LoginJob());
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn0:
                changeTab(Constant.GIRLVIEW);
                break;
            case R.id.btn1:
                changeTab(Constant.SEARCHVIEW);
                break;
            case R.id.btn2:
                changeTab(Constant.VARIETYVIEW);
                break;
            case R.id.btn3:
                changeTab(Constant.HOMEBOYVIEW);
                break;
            case R.id.btn4:
                changeTab(Constant.FAMOUSEVIEW);
                break;
        }
    }
    private void changeTab(int index){
        curTabIndex = index;
        clearSelections();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        hideFragments(fragmentTransaction);
        switch (curTabIndex){
            case Constant.GIRLVIEW:
                img0.setBackgroundResource(R.mipmap.btn_nvu_bottom_1);
                if(girlFragment==null){
                    girlFragment=new GirlFragment();
                    fragmentTransaction.add(R.id.fragmentLayout,girlFragment,girlFragment.getClass().getSimpleName());
                }
                else{
                    fragmentTransaction.show(girlFragment);
                    girlFragment.reflashPage();
                }
                Log.d("test","girlFragment");
                break;
            case Constant.SEARCHVIEW:
                img1.setBackgroundResource(R.mipmap.btn_search_botton_1);
                if(searchlFragment==null){
                    searchlFragment=new SearchlFragment();
                    fragmentTransaction.add(R.id.fragmentLayout,searchlFragment);
                }
                else{
                    fragmentTransaction.show(searchlFragment);
                }
                Log.d("test","searchlFragment");
                break;
            case Constant.VARIETYVIEW:
                img2.setBackgroundResource(R.mipmap.btn_arts_bottom_1);
                if(varietylFragment==null){
                    varietylFragment=new VarietylFragment();
                    fragmentTransaction.add(R.id.fragmentLayout,varietylFragment);
                }
                else{
                    fragmentTransaction.show(varietylFragment);
                    varietylFragment.reflashPage();
                }
                Log.d("test","varietylFragment");
                break;
            case Constant.HOMEBOYVIEW:
                img3.setBackgroundResource(R.mipmap.btn_otaku_bottom_1);
                if(homeboyFragment==null){
                    homeboyFragment=new HomeboyFragment();
                    fragmentTransaction.add(R.id.fragmentLayout,homeboyFragment);
                }
                else{
                    fragmentTransaction.show(homeboyFragment);
                    homeboyFragment.reflashPage();
                }
                Log.d("test","homeboyFragment");
                break;
            case Constant.FAMOUSEVIEW:
                img4.setBackgroundResource(R.mipmap.btn_large_bottom_1);
                if(famouseFragment==null){
                    famouseFragment=new FamouseFragment();
                    fragmentTransaction.add(R.id.fragmentLayout,famouseFragment);
                }
                else{
                    fragmentTransaction.show(famouseFragment);
                    famouseFragment.reflashPage();
                }
                Log.d("test","famouseFragment");
                break;
        }
        fragmentTransaction.commit();
    }
    public void hideFragments(FragmentTransaction transaction)
    {
        if (girlFragment != null)
        {
            transaction.hide(girlFragment);
        }
        if(searchlFragment!=null)
        {
            transaction.hide(searchlFragment);
        }
        if(famouseFragment!=null)
        {
            transaction.hide(famouseFragment);
        }
        if(homeboyFragment!=null)
        {
            transaction.hide(homeboyFragment);
        }
        if(varietylFragment!=null)
        {
            transaction.hide(varietylFragment);
        }
    }
    public void clearSelections()
    {
        img0.setBackgroundResource(R.mipmap.btn_nvu_bottom_0);
        img1.setBackgroundResource(R.mipmap.btn_search_botton_0);
        img2.setBackgroundResource(R.mipmap.btn_arts_bottom_0);
        img3.setBackgroundResource(R.mipmap.btn_otaku_bottom_0);
        img4.setBackgroundResource(R.mipmap.btn_large_bottom_0);

    }
}
