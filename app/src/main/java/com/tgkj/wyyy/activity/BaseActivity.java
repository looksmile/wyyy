package com.tgkj.wyyy.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.dialog.Vip2Dialog;
import com.tgkj.wyyy.dialog.VipDialog;
import com.tgkj.wyyy.dialog.WaitingProgressDialog;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.pay.GeneralPay;

import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/12/15.
 */
public class BaseActivity extends FragmentActivity {
    private WaitingProgressDialog mWaiting;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication myApplication = (MyApplication)getApplication();
        myApplication.addActivity(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        MyApplication application = (MyApplication) getApplication();
        application.removeActivity(this);
    }
    public void showToast(String content) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
    }

    public void onEventMainThread(Object event) {
    }

    public void playVideo(String url, boolean showAll){
        playOnce();
        Intent intent = new Intent(BaseActivity.this, PlayerActivity.class);
        intent.putExtra("URL", url);
        intent.putExtra("ALL", showAll);
        startActivity(intent);
    }


    public void showVipDialog(){
        new VipDialog(this).show();
    }
    public void startWaiting(String msg) {
        if (mWaiting == null) {
            mWaiting = new WaitingProgressDialog(this);
            mWaiting.setCancelable(false);
            mWaiting.setCanceledOnTouchOutside(false);
        }
        if(!TextUtils.isEmpty(msg)){
            mWaiting.setMessage(msg);
        }
        mWaiting.show();
    }

    public void stopWaiting() {
        if (mWaiting != null && mWaiting.isShowing() && !isFinishing()) {
            mWaiting.dismiss();
            mWaiting = null;
        }
    }

    public void showOverDialog(String title){
        new Vip2Dialog(this, title).show();
    }

    private void playOnce(){
        DataCenter.getInstance().rest_play--;
        mSharedPreferences = MyApplication.getInstance().getSharedPreferences("userInfo", 0);
        mEditor = mSharedPreferences.edit();
        mEditor.putInt("rest_play", DataCenter.getInstance().rest_play);
        mEditor.commit();
    }
}
