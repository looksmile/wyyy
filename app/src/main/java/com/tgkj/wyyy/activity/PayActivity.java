package com.tgkj.wyyy.activity;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tgkj.wyyy.Constant;
import com.tgkj.wyyy.MyApplication;
import com.tgkj.wyyy.R;
import com.tgkj.wyyy.events.FreeVideoEvent;
import com.tgkj.wyyy.events.GetVipItemEvent;
import com.tgkj.wyyy.events.PayEvent;
import com.tgkj.wyyy.info.DataCenter;
import com.tgkj.wyyy.info.VideoInfo;
import com.tgkj.wyyy.jobs.GetFreeVideoJob;
import com.tgkj.wyyy.jobs.GetVipItemJob;
import com.tgkj.wyyy.jobs.LoginJob;
import com.tgkj.wyyy.pay.GeneralPay;
import com.tgkj.wyyy.player.VideoPlayView;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogRecord;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by apple on 16/3/1.
 */
public class PayActivity extends BaseActivity implements View.OnClickListener{

    private TextView tv_buy_1, tv_buy_2;
    private Button btn_buy_1, btn_buy_2;
    private Context context;
    private VideoPlayView player;
    RelativeLayout loading_progress;
    int curIndex;
    List<VideoInfo> urlList;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        startWaiting(getResources().getString(R.string.waitMsg));
        MyApplication.getJobManager().addJobInBackground(new GetVipItemJob());
        MyApplication.getJobManager().addJobInBackground(new GetFreeVideoJob());
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loading_progress.setVisibility(View.VISIBLE);
        if(player.isPlaying())
            loading_progress.setVisibility(View.GONE);
    }

    private void initView(){

        context = this;
        curIndex = 0;
        loading_progress = (RelativeLayout) findViewById(R.id.course_surfaceview_layout_loadinglogo);
        tv_buy_1 = (TextView) findViewById(R.id.tv_buy_1);
        tv_buy_2 = (TextView) findViewById(R.id.tv_buy_2);
        btn_buy_1 = (Button) findViewById(R.id.btn_buy_1);
        btn_buy_2 = (Button) findViewById(R.id.btn_buy_2);

        btn_buy_1.setOnClickListener(this);
        btn_buy_2.setOnClickListener(this);

        tv_buy_1.setVisibility(View.GONE);
        tv_buy_2.setVisibility(View.GONE);

        player = (VideoPlayView) findViewById(R.id.video_player);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_buy_1:
                if(DataCenter.getInstance().vips.size() != 2){
                    showToast("数据请求异常");
                    return;
                }
                GeneralPay.getInstance().showPayDialog(context, Constant.QUA_YEAR);
                break;
            case R.id.btn_buy_2:
                if(DataCenter.getInstance().vips.size() != 2){
                    showToast("数据请求异常");
                    return;
                }
                GeneralPay.getInstance().showPayDialog(context, Constant.HALF_YEAR);
                break;
        }
    }

    public void onEventMainThread(FreeVideoEvent event) {
        if (event.isSuccess) {
            urlList = DataCenter.getInstance().vipVideoList;

            player.setVideoURI(Uri.parse(urlList.get(curIndex).Address));

            curIndex = (curIndex + 1) % urlList.size();

            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    player.start();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loading_progress.setVisibility(View.GONE);
                        }
                    }, 200);
                }
            });

            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    loading_progress.setVisibility(View.VISIBLE);
                    player.setVideoURI(Uri.parse(urlList.get(curIndex).Address));
                    curIndex = (curIndex + 1) % urlList.size();
                }
            });
        }
    }

    public void onEventMainThread(GetVipItemEvent event){
        stopWaiting();
        if(event.isSuccess){
            int b = -1, e = -1;
            String temp;
            tv_buy_1.setText(DataCenter.getInstance().vips.get(1).getDesc());
            tv_buy_2.setText(DataCenter.getInstance().vips.get(0).getDesc());

            String s1 = DataCenter.getInstance().vips.get(1).getDesc();
            Pattern p = Pattern.compile("[^0-9]");
            Matcher m = p.matcher(s1);
            temp = m.replaceAll("").trim();
            SpannableString string = new SpannableString(s1);
            string.setSpan(new ForegroundColorSpan(Color.RED),s1.indexOf(temp), s1.indexOf(temp) + temp.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            tv_buy_1.setText(string);

            String s2 = DataCenter.getInstance().vips.get(0).getDesc();
            m = p.matcher(s2);
            temp = m.replaceAll("").trim();
            string = new SpannableString(s2);
            string.setSpan(new ForegroundColorSpan(Color.RED),s2.indexOf(temp), s2.indexOf(temp) + temp.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            tv_buy_2.setText(string);

            tv_buy_1.setVisibility(View.VISIBLE);
            tv_buy_2.setVisibility(View.VISIBLE);
        }
    };

    public void onEventMainThread(PayEvent event){
        stopWaiting();
        if(event.isSuccess()){
            MyApplication.getJobManager().addJobInBackground(new LoginJob());
        }
    }

}
