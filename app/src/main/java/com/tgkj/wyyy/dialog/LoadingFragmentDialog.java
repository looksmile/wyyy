package com.tgkj.wyyy.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tgkj.wyyy.R;


/**
 * fragment里使用的dialog
 * Created by Administrator on 2015/11/17.
 */
public class LoadingFragmentDialog extends DialogFragment {

    private  String msg;

    public LoadingFragmentDialog(){
        setStyle(STYLE_NO_TITLE, R.style.TGDialogTheme);
    }

    public void setMsg(String msg){
        this.msg = msg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.progress_dialog_layout, null);
        TextView tv = (TextView) layout.findViewById(R.id.dialog_msg);
        tv.setText(msg);
        return layout;
    }
}
