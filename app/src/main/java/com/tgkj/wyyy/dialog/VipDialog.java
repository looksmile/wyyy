package com.tgkj.wyyy.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tgkj.wyyy.R;
import com.tgkj.wyyy.activity.BaseActivity;
import com.tgkj.wyyy.activity.PayActivity;
import com.tgkj.wyyy.activity.PlayerActivity;

/**
 * Created by apple on 16/3/7.
 */
public class VipDialog extends Dialog {

    private BaseActivity context;

    public VipDialog(Context context) {
        super(context, R.style.TGDialogTheme);
        this.context = (BaseActivity) context;
        if(context instanceof  PlayerActivity) {
            this.context = (PlayerActivity) context;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.vip_dialog_layout, null);
        setContentView(layout);

        TextView open_vip = (TextView) layout.findViewById(R.id.open_vip);
        open_vip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(context instanceof PlayerActivity)
                    context.finish();
                context.startActivity(new Intent(context, PayActivity.class));
                dismiss();
            }
        });
    }
}
