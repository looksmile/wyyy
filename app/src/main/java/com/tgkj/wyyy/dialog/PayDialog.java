package com.tgkj.wyyy.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.tgkj.wyyy.R;

/**
 * Created by apple on 16/3/1.
 */
public class PayDialog extends Dialog{
    private PayDialog(Context context, int styleId) {
        super(context, styleId);
    }

    //微信支付回调接口
    public interface OnWXpayListener {
        void onWXpayClick();
    }

    //支付宝支付回调接口
    public interface OnAlipayListener {
        void onAlipayClick();
    }

    //PP钱包支付接口
    public interface OnPPPayListener {
        void onPPPayClick();
    }

    public static class Builder{

        private Context context;
        private OnWXpayListener wxpayListener;
        private OnAlipayListener alipayListener;
        private OnPPPayListener ppPayListener;
        private Button btn_alipay, btn_weixin, btn_pppay;

        public Builder(Context ctx) {
            context = ctx;
        }

        public Builder setWXpayListener(OnWXpayListener listener) {
            wxpayListener = listener;
            return this;
        }

        public Builder setAlipayListener(OnAlipayListener listener) {
            alipayListener = listener;
            return this;
        }

        public Builder setPPPayListener(OnPPPayListener listener) {
            ppPayListener = listener;
            return this;
        }

        public PayDialog build(){
            final PayDialog dialog = new PayDialog(context, R.style.TGDialogTheme);
            dialog.setCanceledOnTouchOutside(false);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.pay_dialog_layout, null);
            dialog.setContentView(layout);
            btn_alipay = (Button) layout.findViewById(R.id.btn_ali_pay);
            btn_weixin = (Button) layout.findViewById(R.id.btn_weixin_pay);
            btn_pppay = (Button) layout.findViewById(R.id.btn_pp_pay);

            btn_alipay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(alipayListener != null){
                        alipayListener.onAlipayClick();
                    }
                    dialog.dismiss();
                }
            });

            btn_weixin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(wxpayListener != null){
                        wxpayListener.onWXpayClick();
                    }
                    dialog.dismiss();
                }
            });

            btn_pppay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(ppPayListener != null){
                        ppPayListener.onPPPayClick();
                    }
                    dialog.dismiss();
                }
            });

            return dialog;
        }

    }

}
