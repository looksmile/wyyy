package com.tgkj.wyyy.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tgkj.wyyy.R;


public class WaitingProgressDialog extends Dialog{

	private Context context;
	private String msg = "请稍后";

	public WaitingProgressDialog(Context context) {
        super(context, R.style.TGDialogTheme);
        this.context = context;
	}
	
	public void setMessage(String msg){
		this.msg = msg;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		init();
	}
	
	private void init(){
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.progress_dialog_layout, null);
		setContentView(layout);
		
		TextView tv = (TextView) layout.findViewById(R.id.dialog_msg);
		tv.setText(msg);
	}

}
