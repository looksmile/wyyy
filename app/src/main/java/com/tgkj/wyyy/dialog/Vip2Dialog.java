package com.tgkj.wyyy.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tgkj.wyyy.R;
import com.tgkj.wyyy.activity.BaseActivity;
import com.tgkj.wyyy.activity.PayActivity;
import com.tgkj.wyyy.activity.PlayerActivity;

/**
 * Created by apple on 16/3/8.
 */
public class Vip2Dialog extends Dialog {
    private BaseActivity context;
    private String title_s;

    public Vip2Dialog(Context context, String title) {
        super(context, R.style.TGDialogTheme);
        this.context = (BaseActivity) context;
        this.title_s = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.vip2_dialog_layout, null);
        setContentView(layout);

        Button open_vip = (Button) layout.findViewById(R.id.open_vip);
        TextView title = (TextView) layout.findViewById(R.id.title);
        title.setText(title_s);
        open_vip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, PayActivity.class));
                dismiss();
            }
        });
    }
}
