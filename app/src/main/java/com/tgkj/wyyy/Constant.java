package com.tgkj.wyyy;

public class Constant {

    public static final String ProductID = "10";//产品id

    public static final String ServerUrl = "http://kplayer.zjdb.cn/";
    public static final String bitmap_url = "http://kplayer.zjdb.cn:88/Storage/";
    public static final String H5Url = "http://hgame.zjdb.cn/guest/login?platform=6&openid=";//H平台地址

    public static final boolean ForTest = false;//测试时，商品价格都为1分钱

//    public static final String ServerUrl = "http://183.131.146.146:8081/";//接口测试服务器地址
//    public static final String bitmap_url = "http://183.131.146.146:8090/Storage/";//资源测试服务器
//    public static final String H5Url = "http://183.131.146.146/guest/login?platform=6&openid=";//H平台地址

    public final static int HANDLER_WHAT_DOWNLOAD_RESOURCE = 1002; //APK资源下载
    public final static int HANDLER_WHAT_DOWNLOAD_RESOURCE_EXCEPTION = 1004; //apk资源下载异常

    public static final String md5 = "s4c8ee9a37b50b447f0758f95491dee6";
    public static final String des = "gw9I56dl";

	public static boolean isDownloading = false;
	
	public static String ChannelID;//渠道号
    public static String VERSION_NAME = "0";
	public static String LATEST_VERSION = "0";//最新版本号
	public static String NEW_VERSION_URL; //新版本下载地址

    public static final int PAY_TYPE_ALIPAY = 1;
    public static final int PAY_TYPE_WXPAY = 3;
    public static final int PAY_TYPE_PPPAY = 4;


    //一元包起止时间
    public static final String ONE_YUAN_BEGIN = "2015-12-22 23:59:59";
    public static final String ONE_YUAN_END = "2016-02-01 23:59:59";

    //登录用户信息的Key
    public static final String USER_ACCOUNT = "userAccount";
    public static final String USER_PASSWORD = "userPassword";

    public static final int GIRLVIEW = 0;
    public static final int SEARCHVIEW = 1;
    public static final int VARIETYVIEW = 2;
    public static final int HOMEBOYVIEW = 3;
    public static final int FAMOUSEVIEW = 4;

    //包时类型
    public static final int QUA_YEAR = 0; //包三个月
    public static final int HALF_YEAR = 1;  //包半年

}
